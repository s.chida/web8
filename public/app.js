function openForm() {
  document.getElementById("myForm").style.display = "block";
}

function closeForm() {
  document.getElementById("myForm").style.display = "none";
}

function sendForm() {
  let name = document.getElementsByName("name")[0].value;
  let email = document.getElementsByName("email")[0].value;
  let message = document.getElementsByName("message")[0].value;
  
  $.ajax({
    url: "https://api.slapform.com/chida2001@mail.ru",
    dataType: "json",
    method: "POST",
    data: {
      name: name,
      email: email,
      message: message,
    },
    success: function (response) {
      console.log("Спасибо за отправку", response);
      closeForm();
      document.getElementsByClassName("message")[0].innerHTML = "Спасибо за отправку";
    },
    error: function () {
      document.getElementsByClassName("error")[0].innerHTML = "Ошибка";
    },
  });
  return false;
}
